{ config, lib, pkgs, ... }:

{
	imports = [ 
		<nixpkgs/nixos/modules/installer/scan/not-detected.nix>
	];

	boot.initrd.availableKernelModules = [ "usbhid" ];
	boot.initrd.kernelModules = [ ];
	boot.kernelModules = [ ];
	boot.extraModulePackages = [ ];

	boot.loader.grub.enable = false;
	boot.loader.raspberryPi.enable = true;
	boot.loader.raspberryPi.version = 4;
	boot.kernelPackages = pkgs.linuxPackages_rpi4;

	fileSystems."/" = { 
		device = "/dev/disk/by-label/NIXOS_SD";
		fsType = "ext4";
	};

	fileSystems."/boot" = {
		device = "/dev/disk/by-label/FIRMWARE";
		fsType = "vfat";
	};

	swapDevices = [{ 
		device = "/swapfile";
		size = 2048;
	}];

	nix.maxJobs = lib.mkDefault 4;
	powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
}
