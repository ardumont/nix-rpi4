## Preparations

- Boot a [working aarch64 distribution](https://jamesachambers.com/raspberry-pi-ubuntu-server-18-04-2-installation-guide/) onto the Raspberry Pi 4.
- Change the settings in `secrets.nix`, including Wireless endpoints.
	- **IMPORTANT**: You need to set up a static IP under `networkAddr.address`

## Building the image

```sh
# Install Nix
curl https://nixos.org/nix/install | sh

# Clone this repository
git clone https://gitlab.com/diamondburned/nix-rpi4
cd nix-rpi4

# Build the image
nix-build '<nixpkgs/nixos>' -A config.system.build.sdImage -I nixos-config=./sd-image.nix
```

## Installation

Flash the built image onto the micro SD card, plug it in, then follow
"Remote deployment".

## Remote deployment

```sh
./deploy switch
```

**IMPORTANT**: The Nix files must not import anything outside the directory
(except for Nix channels that are already in the target).
